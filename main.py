"this module contains the main function"
import sys
import os
sys.path.insert(2, os.path.join(os.getcwd(),'ipl_analytics/csv'))
from matches_played_per_year import compute_and_plot_matches_played_per_year
from matches_won_by_teams import compute_and_plot_matches_won_by_teams
from extras import compute_and_plot_extras_2016
from economic_bowlers import compute_and_plot_economic_bowler_2015
from win_loss_stats import compute_and_plot_wins_and_losses_percentage
from helper_func import extract_matches
from helper_func import extract_deliveries

def main():
    """ this is the main method"""
    
    economic_bowler_year = 2019

    extras_year = 2019

    matches = extract_matches('data/matches.csv')

    deliveries = extract_deliveries('data/deliveries.csv')

    compute_and_plot_matches_played_per_year(matches)

    compute_and_plot_matches_won_by_teams(matches)

    compute_and_plot_extras_2016(matches, deliveries, extras_year)

    compute_and_plot_economic_bowler_2015(matches, deliveries, economic_bowler_year)

    compute_and_plot_wins_and_losses_percentage(matches)


if __name__ == "__main__":
    main()
