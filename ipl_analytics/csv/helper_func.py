""" This module contains the helper functions """
import csv

def extract_matches(matches_csv):
    """  this method reads the file and returns the list of dictionaries """
    file_csv = open(matches_csv)

    match_obj = csv.DictReader(file_csv)

    match = [dict(each_match) for each_match in match_obj]

    return match


def extract_deliveries(deliveries_csv):
    """ this method reads the file and returns the list of dictionaries """

    file_csv = open(deliveries_csv)

    deliveries_obj = csv.DictReader(file_csv)

    delivery = [dict(each_delivery) for each_delivery in deliveries_obj]

    return delivery


def get_year_id_range(matches, year):
    """
    returns a dictionary where keys are year and values are lists of ids of the corresponding year .
    """

    dict_id_range = {}

    for each_match in matches:

        if each_match['season'] == str(year):

            dict_id_range[each_match['id']] = 1
            
        else:

            pass
            
    print(dict_id_range)
    
    if bool(dict_id_range):

        return dict_id_range

    return "Data not available"