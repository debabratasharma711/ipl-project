""" show wins loss and tie plot of a team and also its winning percentage over the years """
import matplotlib.pyplot as plt


def win_loss_percentage(matches, team_name):
    """ returns a dictionary of lists where key is
    the years and value is a list of wins, loss and ties
    """
    temp_years_stats_unsorted = {}

    win_loss_tie_count = {}

    win_percentage = {}

    teams = set()

    for each_match in matches:

        teams.add(each_match['team1'])

        teams.add(each_match['team2'])

    if team_name not in teams:

        print("Please enter a valid name.")

        return "Please enter a valid name.", ""

    for each_match in matches:

        if each_match['team1'] == 'Rising Pune Supergiant' or each_match['team2'] == 'Rising Pune Supergiant':

            each_match['winner'] = 'Rising Pune Supergiants'

        if each_match['season'] not in temp_years_stats_unsorted:

            temp_years_stats_unsorted[each_match['season']] = [0, 0, 0]
                # 0th, 1st, 2nd indices are wins, losses, ties count resp

        if team_name in (each_match['team1'], each_match['team2']):

            if each_match['winner'] == team_name:

                temp_years_stats_unsorted[each_match['season']][0] += 1

            elif each_match['result'] == 'no result':

                temp_years_stats_unsorted[each_match['season']][2] += 1

            else:

                temp_years_stats_unsorted[each_match['season']][1] += 1

    win_loss_tie_count = dict(sorted(temp_years_stats_unsorted.items(), key=lambda x: x[0]))

    for year, stat in win_loss_tie_count.items():

        if stat[0]+stat[1]+stat[2] > 0:

            win_percentage[year] = stat[0]/(stat[0]+stat[1]+stat[2]) * 100

        else:

            win_percentage[year] = 0

    print(win_loss_tie_count, "\n", win_percentage)

    return win_loss_tie_count, win_percentage


def plot_win_loss_stats(year_win_loss_tie_count):
    """ plots a stacked bar chart of wins losses and ties of a team over the years """
    seasons = list(year_win_loss_tie_count)

    set_bottom = [0]*len(seasons)

    set_legend = ['wins', 'loss', 'ties']

    plt.figure(figsize=(30, 5))

    for i in range(0, 3):

        win_loss_tie_arr_to_plot = [year_win_loss_tie_count[years][i] \
                                    for years in year_win_loss_tie_count]

        print(win_loss_tie_arr_to_plot)

        plt.bar(seasons, win_loss_tie_arr_to_plot, width=0.3, bottom=set_bottom)

        set_bottom = [set_bottom[i]+win_loss_tie_arr_to_plot[i] \
                      for i in range(0, len(win_loss_tie_arr_to_plot))]

    plt.xlabel("Season")

    plt.ylabel("Win/Loss/Tie")

    plt.title("Number of wins, loss and ties over the years")

    plot_legend = plt.legend(set_legend, loc=2, ncol=1)

    plot_legend.get_frame().set_alpha(0.2)

    plt.show()


def plot_win_percentage(win_percentage):
    """ returns a plot of win % of the given team over the years """
    plt.figure(figsize=(30, 10))

    # print(win_percentage)

    title = "Win percentage over the years"

    seasons = list(win_percentage)

    win_percentage = list(win_percentage.values())

    plt.plot(seasons, win_percentage)

    plt.xlabel('seasons')

    plt.ylabel('win %')

    plt.ylim(0, 100)

    plt.title(title)

    plt.show()


def compute_and_plot_wins_and_losses_percentage(matches):
    """calls plotting function and function that returns data required for plotting"""

    team_name = str(input("Enter one of the following name of teams to see the stats of wins and losses of that team over the years\n\n\nnote: Input is case sensitive \n\n "))

    year_win_loss_tie_count, win_percentage = win_loss_percentage(matches, team_name)

    if year_win_loss_tie_count != "Please enter a valid name.":

        plot_win_loss_stats(year_win_loss_tie_count)

        plot_win_percentage(win_percentage)
