"""module plots a bar graph between IPL seasons and the number of matches played in each season"""
import matplotlib.pyplot as plt


def matches_played_per_year(matches):
    '''returns dictionary where key is years and and value is number of matches played'''

    dict_matches_played_per_year = {}

    for each_match in matches:

        if each_match['season'] not in dict_matches_played_per_year:

            dict_matches_played_per_year[each_match['season']] = 1

        else:

            dict_matches_played_per_year[each_match['season']] += 1

    print(dict_matches_played_per_year)

    return dict_matches_played_per_year


def plot_matches_played_per_year(matches_per_year):
    """ returns plot matches vs season"""
    plt.figure(figsize=(30, 5))

    plt.bar(sorted(matches_per_year.keys()), matches_per_year.values(),\
            align='center', width=0.5, color='g')

    plt.xlabel('Season')

    plt.ylabel('Number of matches played')

    plt.title('Number of matches played in every IPL season')

    plt.show()



def compute_and_plot_matches_played_per_year(matches):
    "calls plotting function and function that returns data required for plotting"

    matches_per_year = matches_played_per_year(matches)

    plot_matches_played_per_year(matches_per_year)
