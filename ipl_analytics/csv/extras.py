""" functions in this module returns organized data from file object and plots
 a bar graph of extra runs conceded by each team in a  given year """

import matplotlib.pyplot as plt
from helper_func import get_year_id_range


def extras_2016(matches, deliveries, year):
    """ returns extra runs conceded by all the teams in a given year in the form of a dictionary """

    dict_extras = {}

    dict_range_id = get_year_id_range(matches, year)

    if dict_range_id == "Data not available":

        print("Data not available")
        
        return dict_range_id

    else:

        for each_delivery in deliveries:

            if each_delivery['match_id'] in dict_range_id:

                if each_delivery['bowling_team'] not in dict_extras:

                    dict_extras[each_delivery['bowling_team']] = int(each_delivery['extra_runs'])

                else:

                    dict_extras[each_delivery['bowling_team']] += int(each_delivery['extra_runs'])

        print(dict_extras, "\n\n")

        return dict_extras


def plot_extras(extra_runs):
    """ returns plot of extras runs given by teams in the given year """

    plt.figure(figsize=(25, 5))

    plt.bar(extra_runs.keys(), extra_runs.values(), align='center', width=0.4, color='b')

    plt.xlabel('Teams')

    plt.ylabel('Extras in 2016')

    plt.title("Extras By Each Team in IPL season 2016")

    plt.show()


def compute_and_plot_extras_2016(matches, deliveries, year):
    """calls plotting function and function that returns data required for plotting"""
    #year = 2016

    extra_runs = extras_2016(matches, deliveries, year)

    if extra_runs != "Data not available":

        plot_extras(extra_runs)
