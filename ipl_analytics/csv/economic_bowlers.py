""" functions in this module returns organized data from file object and plots
 a a bar chart for most economic bowlers in the given year """


import matplotlib.pyplot as plt
from helper_func import get_year_id_range


def economic_bowlers(matches, deliveries, year):
    '''
    returns a dictionary where keys are names of bowlers and values are their bowling economy
    '''
    dict_bowlers_run_ball_count = {}

    dict_economic_bowlers = {}

    dict_id_range = get_year_id_range(matches, year)

    if dict_id_range == "Data not available":

        print("Data not available")
        
        return dict_id_range


    for each_del in deliveries:

        if each_del['match_id'] in dict_id_range:

            if each_del['bowler'] not in dict_bowlers_run_ball_count:

                dict_bowlers_run_ball_count[each_del['bowler']] = [0, 0, 0]
            # the 0th index-- extra runs conceded and the 1st index-- legal delivery count, the 2nd index-- economy

            if each_del['bye_runs'] == '0' and each_del['legbye_runs'] == '0':
            # check if the run conceded applies against the bowler, if yes, then add runs to the bowler

                dict_bowlers_run_ball_count[each_del['bowler']][0] += int(each_del['total_runs'])

            if each_del['noball_runs'] == '0' \
            and each_del['wide_runs'] == '0' \
            and each_del['is_super_over'] == '0':
            # check if a delivery is legal, if yes increase count of bowls by one

                dict_bowlers_run_ball_count[each_del['bowler']][1] += 1

            if dict_bowlers_run_ball_count[each_del['bowler']][1] != 0:
            # calculate economy for legal deliveries

                dict_bowlers_run_ball_count[each_del['bowler']][2] = \
                dict_bowlers_run_ball_count[each_del['bowler']][0]\
                /(dict_bowlers_run_ball_count[each_del['bowler']][1]/6)

    print(dict_bowlers_run_ball_count)
    
    for player_name, balling_stat in dict_bowlers_run_ball_count.items():

        if balling_stat[1] >= 1:

            dict_economic_bowlers[player_name] = balling_stat[2]

    #print(sorted(dict_economic_bowlers.items(), key=lambda v: (v[1], v[0]))[:10])

    return sorted(dict_economic_bowlers.items(), key=lambda v: (v[1], v[0]))[:10]


def plot_economic_bowlers(most_economic_bowlers, year):
    """ returns plot for top 10 economic bowlers in the given year """

    plt.figure(figsize=(25, 10))

    plt.bar(range(len(most_economic_bowlers)), [bowlers[1] \
            for bowlers in most_economic_bowlers], align='center')

    plt.xticks(range(len(most_economic_bowlers)), [bowlers[0] \
               for bowlers in most_economic_bowlers])

    plt.xticks(rotation=35)

    plt.xlabel('Bowlers')

    plt.ylabel('Economy')

    plt.title("Ten Most Economic Bowlers of IPL season " + str(year))

    plt.show()


def compute_and_plot_economic_bowler_2015(matches, deliveries, year):
    """calls plotting function and function that returns data required for plotting"""

    #year = 2015

    most_economic_bowlers = economic_bowlers(matches, deliveries, year)
    
    if most_economic_bowlers != "Data not available":
        plot_economic_bowlers(most_economic_bowlers, year)
