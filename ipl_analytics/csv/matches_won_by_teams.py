'''functions in this module returns organized data from file object and plots
 a stacked bar chart of matches won by all the teams in all the seasons'''

import matplotlib.pyplot as plt


def matches_won_by_teams(matches):
    '''returns a nested dictionary where keys of the outer dictionary are
    team name and keys of the inner dictionary are the IPL seasons and values
    are the number of wins in that year by that team
    '''

    temp_dict_teams_win_per_years = {}

    dict_teams_win_per_years = {}

    seasons = {}

    for each_match in matches:

        if each_match['season'] not in seasons:

            seasons[each_match['season']] = 0


    for each_match in matches:

        if each_match['result'] == 'no result':

            continue

        if each_match['winner'] == 'Rising Pune Supergiant':

            each_match['winner'] = 'Rising Pune Supergiants'

        if each_match['winner'] not in temp_dict_teams_win_per_years:

            temp_dict_teams_win_per_years[each_match['winner']] = {}

            temp_dict_teams_win_per_years[each_match['winner']] = dict(seasons)

            temp_dict_teams_win_per_years[each_match['winner']][each_match['season']] = 1

        elif each_match['winner'] in temp_dict_teams_win_per_years:

            temp_dict_teams_win_per_years[each_match['winner']][each_match['season']] += 1

    for team_name, win_per_year in temp_dict_teams_win_per_years.items():

        dict_teams_win_per_years[team_name] = dict(sorted(win_per_year.items(), key=lambda x: x[0]))

    print(dict_teams_win_per_years, "\n\n")

    return dict_teams_win_per_years, sorted(list(seasons))


def plot_matches_won_by_teams(matches_won_year, seasons):
    '''returns stacked plot of number of matches won by each teams over the years'''

    plt.figure(figsize=(30, 10))

    teams_list = []

    set_bottom = [0]*len(seasons)

    for team_name, win_values in matches_won_year.items():

        teams_list.append(team_name)

        wins_each_team_by_year = list(win_values.values())

        plt.bar(seasons, wins_each_team_by_year, width=0.3, bottom=set_bottom)

        set_bottom = [set_bottom[i]+wins_each_team_by_year[i] \
                      for i in range(0, len(wins_each_team_by_year))]

    plt.xlabel('seasons')

    plt.ylabel('Number of wins each team by seasons')

    plot_legend = plt.legend(teams_list, loc=1, ncol=3)

    plot_legend.get_frame().set_alpha(0.2)

    plt.title('Number of matches won by each team over the years')

    plt.ylim(0, max(set_bottom)+30)

    plt.show()


def compute_and_plot_matches_won_by_teams(matches):
    "function calls plotting function and function that returns data required for plotting"

    matches_won_year, seasons = matches_won_by_teams(matches)

    plot_matches_won_by_teams(matches_won_year, seasons)
