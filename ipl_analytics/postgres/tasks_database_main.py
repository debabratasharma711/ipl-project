""" This is the main module for postgreSQL queries """
import sys
import os
sys.path.insert(2, os.path.join(os.getcwd(),'..'))
from tasks_database import get_num_matches_per_season,\
                           get_match_won_per_team_per_season,\
                           get_extras_2016, get_most_economic_bowler_2015,\
                           get_win_percentage
from postgres_extraction_utils import create_table
from plot_util import plot_matches_played_per_year,\
                      plot_matches_won_by_teams,\
                      plot_extras, plot_economic_bowlers,\
                      plot_win_percentage
from configparser import ConfigParser

def main():
    """ this is the main function """

    configur = ConfigParser()
    configur.read('config.ini')
    matches = configur['HC_parameters']['csv_1']
    deliveries = configur['HC_parameters']['csv_2']
    extra_run_for_year = configur['HC_parameters']['extra_run_for_year']
    economic_bowlers_for_year = configur['HC_parameters']['economic_bowlers_for_year']
    team_name = configur['HC_parameters']['team_name']
    create_table(matches, deliveries, configur['HC_parameters']['csv_1'], configur['HC_parameters']['csv_2'] )
    matches_played_per_year_data = get_num_matches_per_season(matches)
    plot_matches_played_per_year(matches_played_per_year_data)
    matches_won_by_each_team_a,matches_won_by_each_team_b = get_match_won_per_team_per_season(matches)
    plot_matches_won_by_teams(matches_won_by_each_team_a, matches_won_by_each_team_b)
    extra_runs_conceded_data = get_extras_2016(matches, deliveries, extra_run_for_year)
    if extra_runs_conceded_data != "No data available":
        plot_extras(extra_runs_conceded_data)
    most_ecnomic_bowler_data = get_most_economic_bowler_2015(matches, deliveries, economic_bowlers_for_year)
    if most_ecnomic_bowler_data != "No data available":
        plot_economic_bowlers(most_ecnomic_bowler_data, economic_bowlers_for_year)
    win_percentage_data = get_win_percentage(team_name, matches)
    if win_percentage_data != "No data available":
        plot_win_percentage(win_percentage_data)

if __name__ == '__main__':

    main()