""" In this module each method takes the respective query data from postgres.transform.utils as arguments and returns 
transformed data for plotting\
i/p is input
o/p is output """


def matches_played_per_year_transform(extracted_data):
    """i/p: extracted query data of matches played per year\
    type: list of tuples
    o/p:matches played per year
    type: dictionary"""   
        
    return_extracted_data = {}
    for each in extracted_data:
        return_extracted_data[str(each[0])] = each[1]
    print(return_extracted_data)
    return return_extracted_data
    
    
def matches_won_by_each_team_transform(match_won_per_team_per_season_query, seasons_query):
    """ i/p: a. extracted query data of matches won by each team every year\
             b. extracted query data of seasons of IPL\
        type: lists of tuples\
        o/p: a. match_won: matches won by each team every year
             b. list(sorted(seasons_dict): seasons
        type: a. match_won: dictionary of dictionaries
              b. list(sorted(seasons_dict): list"""   
    
    seasons_dict = {}
    for each_season in seasons_query:
        seasons_dict[str(each_season[0])] = 0
    print("\n\nMatches won by each team over the years: \n\n")
    temp_matches_won = {}
    match_won = {}

    for each in match_won_per_team_per_season_query:
        if each[0] == 'Rising Pune Supergiant':
            temp_matches_won['Rising Pune Supergiants'] = dict(seasons_dict)
            temp_matches_won['Rising Pune Supergiants'][str(each[1])] = each[2]
        elif each[0] == None:
            continue
        elif each[0] not in temp_matches_won:   
            temp_matches_won[each[0]] = dict(seasons_dict)
            temp_matches_won[each[0]][str(each[1])] = each[2]
        else:
            temp_matches_won[each[0]][str(each[1])] = each[2]
    for team_name, win_per_year in temp_matches_won.items():
        match_won[team_name] = dict(sorted(win_per_year.items(), key=lambda x: x[0]))
    print(match_won)
    return match_won, list(sorted(seasons_dict))


def extra_runs_conceded_transform(extracted_data):    
    """i/p:  extracted query data of extra runs by each team in the given year\
        type: list of tuples
        o/p:  matches played per year
        type: dictionary or string depending on condition"""  
    
    if extracted_data != "No data available":
        print("\n\nExtras given by each team in the year 2015\n\n")
        return_extracted_data = {}
        for each in extracted_data:
            return_extracted_data[str(each[0])] = each[1]
        print(return_extracted_data)
        return return_extracted_data
    print(extracted_data)
    return extracted_data


def most_ecnomic_bowler_transform(extracted_data):
    """i/p:  extracted query data of top ten economic bowlers in the given year\
       type: list of tuples
       o/p:  top ten economic bowler
       type: list of tuples or string depending on condition"""  
    
    if extracted_data != "No data available":
        print("\n\nMost economic Bowlers of 2015 are:\n\n")
        most_economic_bowler = []
        for each in extracted_data:
            print(each[0],float(each[1]))
            most_economic_bowler.append((each[0],float(each[1])))
        return most_economic_bowler
    print(extracted_data)
    return extracted_data


def win_percentage_transform(team_name, extracted_data, seasons_query):
    """i/p:  extracted query data of win percentage of a team in each years\
    type: list of tuples
    o/p:  matches played per year
    type: dictionary or string depending on condition"""

    if extracted_data != "No data available":
        temp_transform_data = {}
        return_transform_data = {}
        for each_season in seasons_query:
            temp_transform_data[str(each_season[0])] = 0
        print("\n\n Win '%' of "+ team_name + " over the years:")
        for each in extracted_data:
            print(each[0],float(each[1]),"%")
            temp_transform_data[str(each[0])] = float(each[1])
        return_transform_data = dict(sorted(temp_transform_data.items(), key=lambda x: x[0]))
        return return_transform_data
    print(extracted_data)
    return extracted_data