""" This module contains all the postgreSQL queries"""

import psycopg2
from configparser import ConfigParser
import simplecrypt as sc
import base64


def connection_db():
    """ connect to database
    return type:  object of psycopg2.extensions.cursor class"""
    
    db_password=base64.b64decode(config_reader('db_parameters','db_password'))
    con = psycopg2.connect(dbname=config_reader('db_parameters','db_name'), \
        user=config_reader('db_parameters','db_user'), \
        password=sc.decrypt(config_reader('db_parameters','key'),db_password))
    cursor = con.cursor()
    return cursor


def config_reader(section_name,field_name):
    """ this method reads the specified field from config.ini
    return type: string"""
    
    configur = ConfigParser()
    configur.read('config.ini')
    return configur[section_name][field_name]


def matches_played_per_year(table_matches):
    """return query results for matches played over the years
    return type: list of tuples """
    
    cursor = connection_db()
    cursor.execute('''select season, count(*) from {} group by season\
                    order by season asc;'''.format(table_matches))
    return_matches_played_per_year = cursor.fetchall()
    cursor.close()
    return return_matches_played_per_year


def matches_won_by_each_team(table_matches):
    """ return query results for matches won by all the teams over the years
    return type: list of tuples"""

    cursor = connection_db()
    cursor.execute(""" select winner, season, count(*) from {}\
                    group by winner,season order by winner,season asc; """\
                    .format(table_matches))
    return_matches_won_by_each_team = cursor.fetchall()
    cursor.close()
    return return_matches_won_by_each_team


def extra_runs_conceded(table_matches, table_deliveries, year):
    """ return query results for extra runs conceded by all the teams in a given year
    return type: list of tuples"""
    
    cursor = connection_db()
    cursor.execute(""" select  bowling_team , sum(extra_runs) from {} \
                    inner join {} on {}.match_id={}.id where {}.season = {}\
                    group by bowling_team;""".format(table_deliveries, table_matches,\
                    table_deliveries,table_matches, table_matches, year ))
    if cursor.rowcount != 0:
        return_extra_runs_conceded = cursor.fetchall()

    else:
        return_extra_runs_conceded = "No data available"
    cursor.close()
    return return_extra_runs_conceded


def most_ecnomic_bowler(table_matches, table_deliveries, year):
    """ return query results for most economic bowler of the year provided"""
    
    cursor = connection_db()
    cursor.execute(''' with temp_ball_count as (select {}.bowler as bowler, count(ball)/6.0 as over_count from {} inner join {} on {}.match_id = {}.id where season = {} \
                        and is_super_over = '0' and noball_runs = '0'and wide_runs='0' group by bowler order by bowler), temp_runs as (select bowler, \
                        (sum(wide_runs)+sum(noball_runs)+sum(penalty_runs)+sum(batsman_runs)) as runs from {} inner join {} on {}.match_id = {}.id\
                        where season = {} and is_super_over = '0' group by bowler order by bowler) select temp_runs.bowler, temp_runs.runs/temp_ball_count.over_count\
                        as economy from temp_ball_count, temp_runs where temp_ball_count.bowler = temp_runs.bowler order by economy limit 10;'''.format(table_deliveries, table_deliveries,\
                        table_matches, table_deliveries,table_matches, year, table_deliveries, table_matches, table_deliveries, table_matches, year))

    if cursor.rowcount != 0:
        return_most_ecnomic_bowler = cursor.fetchall()
    else:
        return_most_ecnomic_bowler = "No data available"
    cursor.close()
    return return_most_ecnomic_bowler


def win_percentage(team_name, table_matches):
    """ return query results for win percentage of a team over the years"""
    
    cursor = connection_db()
    cursor.execute('''  with match_count as(select count(*) as matches,\
                        season as seasons from {} where team2='{}' or team1='{}' group by season order by season),\
                        wins as (select count(*) as wins,season as season from {} where winner = '{}' group by season)\
                        select season, (wins.wins*1.0/match_count.matches)*100 as win_percentage from wins, match_count where wins.season=match_count.seasons order by wins.season;'''\
                        .format(table_matches, team_name, team_name, table_matches, team_name))
    if cursor.rowcount != 0:
        return_win_percentage = cursor.fetchall()
    else:
        return_win_percentage = "No data available"
    cursor.close()
    return return_win_percentage


def query_years(table_matches):
    
    cursor = connection_db()
    cursor.execute('''select distinct(season) from {};'''.format(table_matches))
    return_query_years = cursor.fetchall()
    cursor.close()
    return return_query_years
    
def create_table(csv_file_1, csv_file_2, csv_file_1_path, csv_file_2_path):
    
    cursor = connection_db()
    cursor.execute('''create TABLE if not exists {} (id serial not NULL, season smallint,city varchar(50),\
                date date, team1 varchar(50), team2 varchar(50), toss_winner varchar(50),\
                toss_decision varchar(10), result varchar(10), dl_applied smallint,  winner varchar(50),\
                win_by_runs smallint, win_by_wickets smallint, player_of_match varchar(50), venue varchar(100),\
                umpire1 varchar(50), umpire2 varchar(50), umpire3 varchar(50));'''.format(csv_file_1))
    cursor.execute('''create table if not exists {} (match_id smallint, inning smallint, batting_team varchar(50),\
                bowling_team varchar(50), over smallint,ball smallint, batsman varchar(100),\
                non_striker varchar(100), bowler varchar(100), is_super_over smallint, wide_runs smallint,\
                bye_runs smallint, legbye_runs smallint, noball_runs smallint,\
                penalty_runs smallint, batsman_runs smallint, extra_runs smallint, total_runs smallint,\
                player_dismissed varchar(100), dismissal_kind varchar(50), fielder varchar(100));'''.format(csv_file_2))
    
    cursor.execute('''select * from matches;''')

    if cursor.rowcount == 0:    
        cursor.execute('''copy {} from '{}' delimiter ',' csv header;'''.format(csv_file_1, csv_file_1_path))
        cursor.execute('''copy {} from '{}' delimiters ',' csv header;'''.format(csv_file_2, csv_file_2_path))