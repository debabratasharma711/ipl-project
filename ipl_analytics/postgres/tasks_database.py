""" This module invokes methods from respective modules to execute the tasks"""

from postgres_extraction_utils import matches_played_per_year, matches_won_by_each_team, \
                                      extra_runs_conceded, most_ecnomic_bowler, win_percentage,\
                                      query_years
from postgres_transformation_utils import matches_played_per_year_transform,\
                                          matches_won_by_each_team_transform,\
                                          extra_runs_conceded_transform, \
                                          most_ecnomic_bowler_transform,\
                                          win_percentage_transform

def get_num_matches_per_season(table_matches):
    """ invokes methods for data extraction and interpretation of matches played 
    per year 
    type: dictionary"""

    extracted_data = matches_played_per_year(table_matches)
    transformed_data = matches_played_per_year_transform(extracted_data)
    return transformed_data

def get_match_won_per_team_per_season(table_matches):
    """ invokes methods for data extraction and interpretation of matches won by 
    each team over the years
    type: dictionary"""

    extracted_data_wins = matches_won_by_each_team(table_matches)
    extracted_data_year = query_years(table_matches)
    transformed_matches_won, transformed_seasons= matches_won_by_each_team_transform(extracted_data_wins, extracted_data_year)
    return transformed_matches_won, transformed_seasons


def get_extras_2016(table_matches, table_deliveries, year):
    """ method invokes methods for data extraction and interpretation of extra runs in the year 2016
    type: dictionary"""
    
    extracted_data = extra_runs_conceded(table_matches, table_deliveries, year)
    transformed_data = extra_runs_conceded_transform(extracted_data)
    return transformed_data    
    

def get_most_economic_bowler_2015(table_matches, table_deliveries, year):
    """ method invokes methods for data extraction and interpretation of most economic bowlers of 2015
    type: dictionary"""
    
    extracted_data = most_ecnomic_bowler(table_matches, table_deliveries, year)
    transformed_data = most_ecnomic_bowler_transform(extracted_data)
    return transformed_data


def get_win_percentage(team_name, table_matches):
    """ method invokes methods for data extraction and interpretation of win percentage of a team over the years
    type: dictionary """
    
    extracted_data_wins = win_percentage(team_name, table_matches)
    extracted_data_year = query_years(table_matches)
    transformed_data = win_percentage_transform(team_name, extracted_data_wins, extracted_data_year)
    return transformed_data