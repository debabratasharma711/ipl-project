""" this module tests the matches_won_by_teams module """
import sys
import os
sys.path.insert(2, os.path.join(os.getcwd(),'../ipl_analytics/csv'))
sys.path.insert(3, os.path.join(os.getcwd(), '../ipl_analytics/postgres/'))
import csv
import unittest
from matches_won_by_teams import matches_won_by_teams
from helper_func import extract_matches
from tasks_database import get_match_won_per_team_per_season
from postgres_extraction_utils import create_table

print(sys.path)

class MatchWinCount(unittest.TestCase):
    
    def test_matches_csv(self):

        test_match_obj = extract_matches("test_data/test_matches.csv")
        
        expected_output = ({'b': {'2009': 1, '2011': 0, '2013': 2, '2014': 0}, 
                            'c': {'2009': 2, '2011': 0, '2013': 0, '2014': 0}, 
                            'a': {'2009': 1, '2011': 1, '2013': 0, '2014': 0}, 
                            'e': {'2009': 0, '2011': 2, '2013': 0, '2014': 0},
                            'd': {'2009': 0, '2011': 1, '2013': 0, '2014': 0},
                            'x': {'2009': 0, '2011': 0, '2013': 0, '2014': 1}},
                            ['2009', '2011', '2013', '2014'])

        self.assertEqual(matches_won_by_teams(test_match_obj), expected_output)

    def test_matches_sql(self):

        create_table('test_matches','test_deliveries', '/home/debabrata/mountblue/project-1/ipl_boilerplate/test_matches.csv','/home/debabrata/mountblue/project-1/ipl_boilerplate/test_deliveries.csv')
        
        matches_won_by_teams_sql = get_match_won_per_team_per_season('test_matches')
        
        expected_output = ({'a': {'2014': 0, '2011': 1, '2009': 1, '2013': 0}, \
                            'b': {'2014': 0, '2011': 0, '2009': 1, '2013': 2}, \
                            'c': {'2014': 0, '2011': 0, '2009': 2, '2013': 0}, \
                            'd': {'2014': 0, '2011': 1, '2009': 0, '2013': 0},\
                            'e': {'2014': 0, '2011': 2, '2009': 0, '2013': 0}, \
                            'x': {'2014': 1, '2011': 0, '2009': 0, '2013': 0}},\
                            ['2009', '2011', '2013', '2014'])


        self.assertEqual(matches_won_by_teams_sql, expected_output)
        
    
if __name__ == '__main__':
    
    unittest.main()