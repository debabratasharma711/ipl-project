""" this module tests the matches_played_per_year module """
import sys
import os
sys.path.insert(2, os.path.join(os.getcwd(),'../ipl_analytics/csv'))
sys.path.insert(3, os.path.join(os.getcwd(), '../ipl_analytics/postgres/'))
import csv
import unittest
from matches_played_per_year import matches_played_per_year
from helper_func import extract_matches
from tasks_database import get_num_matches_per_season
from postgres_extraction_utils import create_table


class MatchCountTest(unittest.TestCase):

    def test_match_played_per_year_csv(self):


        test_match_obj = extract_matches("test_data/test_matches.csv")
        
        expected_output = {'2009': 4, '2011': 4, '2013': 2, '2014': 1}

        self.assertEqual(matches_played_per_year(test_match_obj), expected_output)

        
    def test_match_played_per_year_sql(self):

        create_table('test_matches','test_deliveries', '/home/debabrata/mountblue/project-1/ipl_boilerplate/test_matches.csv','/home/debabrata/mountblue/project-1/ipl_boilerplate/test_deliveries.csv')
        
        num_matches_per_season_query = get_num_matches_per_season('test_matches')
        
        expected_output = {'2009': 4, '2011': 4, '2013': 2, '2014': 1}

        self.assertEqual(num_matches_per_season_query, expected_output)



if __name__ == '__main__':

    unittest.main()