import sys
import os
sys.path.insert(2, os.path.join(os.getcwd(),'../ipl_analytics/csv'))
sys.path.insert(3, os.path.join(os.getcwd(), '../ipl_analytics/postgres/'))
import unittest
from win_loss_stats import *
from helper_func import *
from tasks_database import get_win_percentage
from postgres_extraction_utils import create_table

class StatsCheck(unittest.TestCase):
    """ testing for range of id's of each season """
    
    def test_incorrect_input(self):

        test_match_obj = extract_matches("test_data/test_matches.csv")

        expected_output = "Please enter a valid name."

        output_1, output_2 = win_loss_percentage(test_match_obj, 'xx')

        self.assertEqual(output_1, expected_output)


    def test_correct_input_csv(self):
        """ testing for range of id's of each season """

        test_match_obj = extract_matches("test_data/test_matches.csv")

        expected_output_1, expected_output_2 = {'2009': [1, 1, 0], '2011': [0, 0, 0], '2013': [2, 0, 0],'2014': [0, 0, 0]}, {'2009': 50.0, '2011': 0, '2013': 100.0, '2014': 0}

        output_1, output_2 = win_loss_percentage(test_match_obj, 'b')

        self.assertEqual(output_1, expected_output_1)

        self.assertEqual(output_2, expected_output_2)
    
    def test_correct_sql(self):

        create_table('test_matches','test_deliveries', '/home/debabrata/mountblue/project-1/ipl_boilerplate/test_matches.csv','/home/debabrata/mountblue/project-1/ipl_boilerplate/test_deliveries.csv')

        win_percentage = get_win_percentage('b', 'test_matches')

        expected_output = {'2009': 50.0, '2011': 0, '2013': 100.0, '2014': 0}

        self.assertEqual(win_percentage, expected_output)

if __name__ == '__main__':

    unittest.main()