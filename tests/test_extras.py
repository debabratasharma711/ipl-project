""" this module checks the extras module """
import sys
import os
sys.path.insert(2, os.path.join(os.getcwd(),'../ipl_analytics/csv'))
sys.path.insert(3, os.path.join(os.getcwd(), '../ipl_analytics/postgres/'))
import unittest
from extras import extras_2016
from helper_func import *
from tasks_database import get_extras_2016
from postgres_extraction_utils import create_table

class ExtrasCount(unittest.TestCase):
    """ testing for range of id's of each season """

    def test_extras_csv(self):
        """ testing for extra runs of each season """

        test_match_obj = extract_matches("test_data/test_matches.csv")

        test_delivery_obj = extract_deliveries("test_data/test_deliveries.csv")

        expected_output = {'b': 1, 'a': 1, 'c': 2, 'd': 0}

        self.assertEqual(extras_2016(test_match_obj, test_delivery_obj, 2009), expected_output)
    
    def test_extras_csv_invalid_input(self):
        """ testing for invalid input """
        test_match_obj = extract_matches("test_data/test_matches.csv")

        test_delivery_obj = extract_deliveries("test_data/test_deliveries.csv")

        expected_output = "Data not available"

        self.assertEqual(extras_2016(test_match_obj, test_delivery_obj, 2019), expected_output)

        
    def test_extras_sql(self):
        """ testing sql query for extra runs """
        create_table('test_matches','test_deliveries', '/home/debabrata/mountblue/project-1/ipl_boilerplate/test_matches.csv','/home/debabrata/mountblue/project-1/ipl_boilerplate/test_deliveries.csv')
        
        get_extras_query = get_extras_2016('test_matches', 'test_deliveries', '2009')
        
        expected_output = {'b': 1, 'a': 1, 'c': 2, 'd': 0}

        self.assertEqual(get_extras_query, expected_output)

    def test_extras_sql_invalid_input(self):
        """ testing sql query for invalid input """
        create_table('test_matches','test_deliveries', '/home/debabrata/mountblue/project-1/ipl_boilerplate/test_matches.csv','/home/debabrata/mountblue/project-1/ipl_boilerplate/test_deliveries.csv')
        
        get_extras_query = get_extras_2016('test_matches', 'test_deliveries', '2019')
        
        expected_output = "No data available"

        self.assertEqual(get_extras_query, expected_output)

if __name__ == '__main__':

    unittest.main()