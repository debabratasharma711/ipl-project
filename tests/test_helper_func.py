""" this module tests the helper_func module """
import sys
import os
sys.path.insert(2, os.path.join(os.getcwd(),'../ipl_analytics/csv'))
sys.path.insert(3, os.path.join(os.getcwd(), '../ipl_analytics/postgres/'))
import unittest
from helper_func import extract_matches
from helper_func import get_year_id_range


class Test(unittest.TestCase):
    """ testing for range of id's of each season """

    def test_year_id_range(self):
        """ testing for range of id's of each season """

        test_match_obj = extract_matches("test_data/test_matches.csv")

        expected_output = {'1': 1, '2': 1, '3': 1, '4': 1}

        self.assertEqual(get_year_id_range(test_match_obj, 2009), expected_output)

    def test_year_id_range_invalid_input(self):
        """ testing for invalid inputs or input for which there is no match id"""
        test_match_obj = extract_matches("test_data/test_matches.csv")

        expected_output = "Data not available"

        self.assertEqual(get_year_id_range(test_match_obj, 2019), expected_output)

if __name__ == '__main__':

    unittest.main()