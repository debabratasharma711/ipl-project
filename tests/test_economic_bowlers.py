""" this module checks the economic_bowler module """
import sys
import os
sys.path.insert(2, os.path.join(os.getcwd(),'../ipl_analytics/csv'))
sys.path.insert(3, os.path.join(os.getcwd(), '../ipl_analytics/postgres/'))
import unittest
from economic_bowlers import economic_bowlers
from helper_func import extract_matches
from helper_func import extract_deliveries
from tasks_database import get_most_economic_bowler_2015
from postgres_extraction_utils import create_table


class EconomicBowlers(unittest.TestCase):
    """ testing for economic bowlers of a season """

    def test_economic_bowler_csv(self):
        """ testing for economic bowlers from csv data """

        test_match_obj = extract_matches("test_data/test_matches.csv")

        test_delivery_obj = extract_deliveries("test_data/test_deliveries.csv")

        expected_output = [('bbc', 0.0), ('abc', 3.0), ('ppp', 12.0), ('yyy', 14.399999999999999), ('xxx', 24.0)]

        self.assertEqual(economic_bowlers(test_match_obj, test_delivery_obj, 2009), expected_output)

    
    def test_economic_bowler_csv_invalid_input(self):
        """ testing for economic bowlers from csv data for invalid input"""

        test_match_obj = extract_matches("test_data/test_matches.csv")

        test_delivery_obj = extract_deliveries("test_data/test_deliveries.csv")

        expected_output = "Data not available"

        self.assertEqual(economic_bowlers(test_match_obj, test_delivery_obj, 2019), expected_output)

    
    def test_economic_bowler_sql(self):
        """ testing for economic bowlers from sql data """

        create_table('test_matches','test_deliveries', '/home/debabrata/mountblue/project-1/ipl_boilerplate/test_matches.csv',\
                    '/home/debabrata/mountblue/project-1/ipl_boilerplate/test_deliveries.csv')

        economic_bowler_query = get_most_economic_bowler_2015('test_matches', 'test_deliveries', 2009)

        expected_output = [('bbc', 0.0), ('abc', 3.0), ('ppp', 12.0), ('yyy', 14.4), ('xxx', 24.0)]

        self.assertEqual(economic_bowler_query,expected_output)

    
    def test_economic_bowler_sql_invalid_input(self):
        """ testing for economic bowlers from sql data for invalid input"""

        create_table('test_matches','test_deliveries', '/home/debabrata/mountblue/project-1/ipl_boilerplate/test_matches.csv',\
                    '/home/debabrata/mountblue/project-1/ipl_boilerplate/test_deliveries.csv')

        economic_bowler_query = get_most_economic_bowler_2015('test_matches', 'test_deliveries', 2019)

        expected_output = "No data available"

        self.assertEqual(economic_bowler_query,expected_output)


if __name__ == '__main__':

    unittest.main()