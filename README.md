
-----This project has three sections. Real time IPL datasets are provided(matches.csv and deliveries.csv). Section one is the execution of the following tasks using an object of file(typically an object of csv.DictReader class) class. Section two is the exection of the same queries using postgres and pycopg2 library. Section three is unit-testing------------------


This project contains the following tasks from the IPL dataset:

    Task 1. Number of matches played in each season of the IPL
    Task 2. Number of matches won by each team in all the seasons of IPL.
    Task 3. Number of extra runs conceded by each team in the year 2016.
    Task 4. Most economic bowlers of 2016.
    Task 5. Win % of a team over the years.

Here is the directory structure(the description may/may not be meaningful based on editors):

    .
    ├── data
    │   ├── deliveries.csv
    │   └── matches.csv
    ├── ipl_analytics
    │   ├── csv
    │   │   ├── economic_bowlers.py
    │   │   ├── extras.py
    │   │   ├── helper_func.py
    │   │   ├── matches_played_per_year.py
    │   │   ├── matches_won_by_teams.py
    │   │   └── win_loss_stats.py
    │   └── postgres
    │       ├── config.ini
    │       ├── postgres_extraction_utils.py
    │       ├── postgres_transformation_utils.py
    │       ├── tasks_database_main.py
    │       └── tasks_database.py
    ├── main.py
    ├── README.md
    ├── requirement.txt
    └── tests
        ├── config.ini
        ├── test_data
        │   ├── test_deliveries.csv
        │   └── test_matches.csv
        ├── test_economic_bowlers.py
        ├── test_extras.py
        ├── test_helper_func.py
        ├── test_matches_played_per_year.py
        ├── test_matches_won_by_teams.py
        └── test_win_loss_stats.py

## CSV files description:

    -matches.csv : contains summary of each match of IPL in the years between 2008 to 2017(inclusive)
    -deliveries.csv : contains details of each delivery of each match of IPL in the years between 2008 to 2017(inclusive)
    -test_matches.csv : dummy matches file for testing 
    -test_deliveries.csv :  dummy deliveries file for testing
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

This project has been implemented in python 3.6 and VSCODE 1.42.1 and OS used ubuntu 18.04

The packages used in the project are enlisted in requirement.txt

To install the packages go to command line and type :'pip3 install requirement.txt'

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## RUN SECTION ONE:

To execute section one tasks, run main.py module from its root directory. 

Change values of variable in main.py to check other results if you wish to.

PS: edge cases are not covered in this project,i.e: If data is not present in the file, it will not throw an error. 


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## RUN SECTION TWO:

Go to config.ini in the postgres directory. Provide the key in the 'key' field.

To execute section two tasks, run tasks_database_main.py module.

Change values of variables in tasks_database_main.py to check other results if you wish to.

PS: edge cases are not covered in this project. If data is not present in the file, it will not throw an error.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## RUN SECTION THREE:

Go to config.ini in the tests directory. Provide the key in the 'key' field.

Each unit testing file contain the testing of data generated for plotting in both section one and section two; i.e, e.g: test_matches_played_per_year.py contains test cases for the data generated for plotting task 1 in both section one and section two.

To check the unit testing files, run each file in the test package individually.

